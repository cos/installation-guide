# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2023-02-26 23:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: partitioning.xml:5
#, no-c-format
msgid "Partitioning for &debian;"
msgstr ""

#. Tag: title
#: partitioning.xml:13
#, no-c-format
msgid "Deciding on &debian; Partitions and Sizes"
msgstr ""

#. Tag: para
#: partitioning.xml:14
#, no-c-format
msgid ""
"At a bare minimum, GNU/&arch-kernel; needs one partition for itself. You can "
"have a single partition containing the entire operating system, "
"applications, and your personal files. Most people feel that a separate swap "
"partition is also a necessity, although it's not strictly true. <quote>Swap</"
"quote> is scratch space for an operating system, which allows the system to "
"use disk storage as <quote>virtual memory</quote>. By putting swap on a "
"separate partition, &arch-kernel; can make much more efficient use of it. It "
"is possible to force &arch-kernel; to use a regular file as swap, but it is "
"not recommended."
msgstr ""

#. Tag: para
#: partitioning.xml:26
#, no-c-format
msgid ""
"Most people choose to give GNU/&arch-kernel; more than the minimum number of "
"partitions, however. There are two reasons you might want to break up the "
"file system into a number of smaller partitions. The first is for safety. If "
"something happens to corrupt the file system, generally only one partition "
"is affected. Thus, you only have to replace (from the backups you've been "
"carefully keeping) a portion of your system. At a bare minimum, you should "
"consider creating what is commonly called a <quote>root partition</quote>. "
"This contains the most essential components of the system. If any other "
"partitions get corrupted, you can still boot into GNU/&arch-kernel; to fix "
"the system. This can save you the trouble of having to reinstall the system "
"from scratch."
msgstr ""

#. Tag: para
#: partitioning.xml:40
#, no-c-format
msgid ""
"The second reason is generally more important in a business setting, but it "
"really depends on your use of the machine. For example, a mail server "
"getting spammed with e-mail can easily fill a partition. If you made "
"<filename>/var/mail</filename> a separate partition on the mail server, most "
"of the system will remain working even if you get spammed."
msgstr ""

#. Tag: para
#: partitioning.xml:48
#, no-c-format
msgid ""
"The only real drawback to using more partitions is that it is often "
"difficult to know in advance what your needs will be. If you make a "
"partition too small then you will either have to reinstall the system or you "
"will be constantly moving things around to make room in the undersized "
"partition. On the other hand, if you make the partition too big, you will be "
"wasting space that could be used elsewhere. Disk space is cheap nowadays, "
"but why throw your money away?"
msgstr ""

#. Tag: title
#: partitioning.xml:67
#, no-c-format
msgid "The Directory Tree"
msgstr ""

#. Tag: para
#: partitioning.xml:68
#, no-c-format
msgid ""
"&debian-gnu; adheres to the <ulink url=\"&url-fhs-home;\">Filesystem "
"Hierarchy Standard</ulink> for directory and file naming. This standard "
"allows users and software programs to predict the location of files and "
"directories. The root level directory is represented simply by the slash "
"<filename>/</filename>. At the root level, all &debian; systems include "
"these directories:"
msgstr ""

#. Tag: entry
#: partitioning.xml:82
#, no-c-format
msgid "Directory"
msgstr ""

#. Tag: entry
#: partitioning.xml:82
#, no-c-format
msgid "Content"
msgstr ""

#. Tag: filename
#: partitioning.xml:88
#, no-c-format
msgid "<filename>bin</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:89
#, no-c-format
msgid "Essential command binaries"
msgstr ""

#. Tag: filename
#: partitioning.xml:91
#, no-c-format
msgid "boot"
msgstr ""

#. Tag: entry
#: partitioning.xml:92
#, no-c-format
msgid "Static files of the boot loader"
msgstr ""

#. Tag: filename
#: partitioning.xml:94
#, no-c-format
msgid "<filename>dev</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:95
#, no-c-format
msgid "Device files"
msgstr ""

#. Tag: filename
#: partitioning.xml:97
#, no-c-format
msgid "<filename>etc</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:98
#, no-c-format
msgid "Host-specific system configuration"
msgstr ""

#. Tag: filename
#: partitioning.xml:100
#, no-c-format
msgid "home"
msgstr ""

#. Tag: entry
#: partitioning.xml:101
#, no-c-format
msgid "User home directories"
msgstr ""

#. Tag: filename
#: partitioning.xml:103
#, no-c-format
msgid "<filename>lib</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:104
#, no-c-format
msgid "Essential shared libraries and kernel modules"
msgstr ""

#. Tag: filename
#: partitioning.xml:106
#, no-c-format
msgid "media"
msgstr ""

#. Tag: entry
#: partitioning.xml:107
#, no-c-format
msgid "Contains mount points for replaceable media"
msgstr ""

#. Tag: filename
#: partitioning.xml:109
#, no-c-format
msgid "<filename>mnt</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:110
#, no-c-format
msgid "Mount point for mounting a file system temporarily"
msgstr ""

#. Tag: filename
#: partitioning.xml:112
#, no-c-format
msgid "proc"
msgstr ""

#. Tag: entry
#: partitioning.xml:113 partitioning.xml:125
#, no-c-format
msgid "Virtual directory for system information"
msgstr ""

#. Tag: filename
#: partitioning.xml:115
#, no-c-format
msgid "root"
msgstr ""

#. Tag: entry
#: partitioning.xml:116
#, no-c-format
msgid "Home directory for the root user"
msgstr ""

#. Tag: filename
#: partitioning.xml:118
#, no-c-format
msgid "<filename>run</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:119
#, no-c-format
msgid "Run-time variable data"
msgstr ""

#. Tag: filename
#: partitioning.xml:121
#, no-c-format
msgid "sbin"
msgstr ""

#. Tag: entry
#: partitioning.xml:122
#, no-c-format
msgid "Essential system binaries"
msgstr ""

#. Tag: filename
#: partitioning.xml:124
#, no-c-format
msgid "<filename>sys</filename>"
msgstr ""

#. Tag: filename
#: partitioning.xml:127
#, no-c-format
msgid "<filename>tmp</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:128
#, no-c-format
msgid "Temporary files"
msgstr ""

#. Tag: filename
#: partitioning.xml:130
#, no-c-format
msgid "<filename>usr</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:131
#, no-c-format
msgid "Secondary hierarchy"
msgstr ""

#. Tag: filename
#: partitioning.xml:133
#, no-c-format
msgid "<filename>var</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:134
#, no-c-format
msgid "Variable data"
msgstr ""

#. Tag: filename
#: partitioning.xml:136
#, no-c-format
msgid "<filename>srv</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:137
#, no-c-format
msgid "Data for services provided by the system"
msgstr ""

#. Tag: filename
#: partitioning.xml:139
#, no-c-format
msgid "<filename>opt</filename>"
msgstr ""

#. Tag: entry
#: partitioning.xml:140
#, no-c-format
msgid "Add-on application software packages"
msgstr ""

#. Tag: para
#: partitioning.xml:145
#, no-c-format
msgid ""
"The following is a list of important considerations regarding directories "
"and partitions. Note that disk usage varies widely given system "
"configuration and specific usage patterns. The recommendations here are "
"general guidelines and provide a starting point for partitioning."
msgstr ""

#. Tag: para
#: partitioning.xml:155
#, no-c-format
msgid ""
"The root partition <filename>/</filename> must always physically contain "
"<filename>/etc</filename>, <filename>/bin</filename>, <filename>/sbin</"
"filename>, <filename>/lib</filename>, <filename>/dev</filename> and "
"<filename>/usr</filename>, otherwise you won't be able to boot. This means "
"that you should provide at least &root-system-size-min;&ndash;&root-system-"
"size-max;MB of disk space for the root partition including <filename>/usr</"
"filename>, or &root-desktop-system-size-min;&ndash;&root-desktop-system-size-"
"max;GB for a workstation or a server installation."
msgstr ""

#. Tag: para
#: partitioning.xml:169
#, no-c-format
msgid ""
"<filename>/var</filename>: variable data like news articles, e-mails, web "
"sites, databases, the packaging system cache, etc. will be placed under this "
"directory. The size of this directory depends greatly on the usage of your "
"system, but for most people will be dictated by the package management "
"tool's overhead. If you are going to do a full installation of just about "
"everything &debian; has to offer, all in one session, setting aside 2 or 3 "
"GB of space for <filename>/var</filename> should be sufficient. If you are "
"going to install in pieces (that is to say, install services and utilities, "
"followed by text stuff, then X, ...), you can get away with 300&ndash;500 "
"MB. If hard drive space is at a premium and you don't plan on doing major "
"system updates, you can get by with as little as 30 or 40 MB."
msgstr ""

#. Tag: para
#: partitioning.xml:185
#, no-c-format
msgid ""
"<filename>/tmp</filename>: temporary data created by programs will most "
"likely go in this directory. 40&ndash;100MB should usually be enough. Some "
"applications &mdash; including archive manipulators, CD/DVD authoring tools, "
"and multimedia software &mdash; may use <filename>/tmp</filename> to "
"temporarily store image files. If you plan to use such applications, you "
"should adjust the space available in <filename>/tmp</filename> accordingly."
msgstr ""

#. Tag: para
#: partitioning.xml:196
#, no-c-format
msgid ""
"<filename>/home</filename>: every user will put his personal data into a "
"subdirectory of this directory. Its size depends on how many users will be "
"using the system and what files are to be stored in their directories. "
"Depending on your planned usage you should reserve about 100MB for each "
"user, but adapt this value to your needs. Reserve a lot more space if you "
"plan to save a lot of multimedia files (pictures, MP3, movies) in your home "
"directory."
msgstr ""

#. Tag: title
#: partitioning.xml:217
#, no-c-format
msgid "Recommended Partitioning Scheme"
msgstr ""

#. Tag: para
#: partitioning.xml:218
#, no-c-format
msgid ""
"For new users, personal &debian; boxes, home systems, and other single-user "
"setups, a single <filename>/</filename> partition (plus swap) is probably "
"the easiest, simplest way to go. The recommended partition type is ext4."
msgstr ""

#. Tag: para
#: partitioning.xml:225
#, no-c-format
msgid ""
"For multi-user systems or systems with lots of disk space, it's best to put "
"<filename>/var</filename>, <filename>/tmp</filename>, and <filename>/home</"
"filename> each on their own partitions separate from the <filename>/</"
"filename> partition."
msgstr ""

#. Tag: para
#: partitioning.xml:233
#, no-c-format
msgid ""
"You might need a separate <filename>/usr/local</filename> partition if you "
"plan to install many programs that are not part of the &debian; "
"distribution. If your machine will be a mail server, you might need to make "
"<filename>/var/mail</filename> a separate partition. If you are setting up a "
"server with lots of user accounts, it's generally good to have a separate, "
"large <filename>/home</filename> partition. In general, the partitioning "
"situation varies from computer to computer depending on its uses."
msgstr ""

#. Tag: para
#: partitioning.xml:244
#, no-c-format
msgid ""
"For very complex systems, you should see the <ulink url=\"&url-multidisk-"
"howto;\"> Multi Disk HOWTO</ulink>. This contains in-depth information, "
"mostly of interest to ISPs and people setting up servers."
msgstr ""

#. Tag: para
#: partitioning.xml:251
#, no-c-format
msgid ""
"With respect to the issue of swap partition size, there are many views. One "
"rule of thumb which works well is to use as much swap as you have system "
"memory. It also shouldn't be smaller than 512MB, in most cases. Of course, "
"there are exceptions to these rules."
msgstr ""

#. Tag: para
#: partitioning.xml:258
#, no-c-format
msgid ""
"As an example, an older home machine might have 512MB of RAM and a 20GB SATA "
"drive on <filename>/dev/sda</filename>. There might be a 8GB partition for "
"another operating system on <filename>/dev/sda1</filename>, a 512MB swap "
"partition on <filename>/dev/sda3</filename> and about 11.4GB on <filename>/"
"dev/sda2</filename> as the Linux partition."
msgstr ""

#. Tag: para
#: partitioning.xml:267
#, no-c-format
msgid ""
"For an idea of the space taken by tasks you might be interested in adding "
"after your system installation is complete, check <xref linkend=\"tasksel-"
"size-list\"/>."
msgstr ""

#. Tag: title
#: partitioning.xml:283
#, no-c-format
msgid "Device Names in Linux"
msgstr ""

#. Tag: para
#: partitioning.xml:284
#, no-c-format
msgid ""
"Linux disks and partition names may be different from other operating "
"systems. You need to know the names that Linux uses when you create and "
"mount partitions. Here's the basic naming scheme:"
msgstr ""

#. Tag: para
#: partitioning.xml:292
#, no-c-format
msgid "The first hard disk detected is named <filename>/dev/sda</filename>."
msgstr ""

#. Tag: para
#: partitioning.xml:297
#, no-c-format
msgid ""
"The second hard disk detected is named <filename>/dev/sdb</filename>, and so "
"on."
msgstr ""

#. Tag: para
#: partitioning.xml:303
#, no-c-format
msgid ""
"The first SCSI CD-ROM is named <filename>/dev/scd0</filename>, also known as "
"<filename>/dev/sr0</filename>."
msgstr ""

#. Tag: para
#: partitioning.xml:312
#, no-c-format
msgid "The first DASD device is named <filename>/dev/dasda</filename>."
msgstr ""

#. Tag: para
#: partitioning.xml:318
#, no-c-format
msgid ""
"The second DASD device is named <filename>/dev/dasdb</filename>, and so on."
msgstr ""

#. Tag: para
#: partitioning.xml:326
#, no-c-format
msgid ""
"The partitions on each disk are represented by appending a decimal number to "
"the disk name: <filename>sda1</filename> and <filename>sda2</filename> "
"represent the first and second partitions of the first SCSI disk drive in "
"your system."
msgstr ""

#. Tag: para
#: partitioning.xml:333
#, no-c-format
msgid ""
"Here is a real-life example. Let's assume you have a system with 2 SCSI "
"disks, one at SCSI address 2 and the other at SCSI address 4. The first disk "
"(at address 2) is then named <filename>sda</filename>, and the second "
"<filename>sdb</filename>. If the <filename>sda</filename> drive has 3 "
"partitions on it, these will be named <filename>sda1</filename>, "
"<filename>sda2</filename>, and <filename>sda3</filename>. The same applies "
"to the <filename>sdb</filename> disk and its partitions."
msgstr ""

#. Tag: para
#: partitioning.xml:344
#, no-c-format
msgid ""
"Note that if you have two SCSI host bus adapters (i.e., controllers), the "
"order of the drives can get confusing. The best solution in this case is to "
"watch the boot messages, assuming you know the drive models and/or "
"capacities."
msgstr ""

#. Tag: para
#: partitioning.xml:351
#, no-c-format
msgid ""
"Linux represents the primary partitions as the drive name, plus the numbers "
"1 through 4. For example, the first primary partition on the first drive is "
"<filename>/dev/sda1</filename>. The logical partitions are numbered starting "
"at 5, so the first logical partition on that same drive is <filename>/dev/"
"sda5</filename>. Remember that the extended partition, that is, the primary "
"partition holding the logical partitions, is not usable by itself."
msgstr ""

#. Tag: para
#: partitioning.xml:363
#, no-c-format
msgid ""
"The partitions on each disk are represented by appending a decimal number to "
"the disk name: <filename>dasda1</filename> and <filename>dasda2</filename> "
"represent the first and second partitions of the first DASD device in your "
"system."
msgstr ""

#. Tag: title
#: partitioning.xml:378
#, no-c-format
msgid "&debian; Partitioning Programs"
msgstr ""

#. Tag: para
#: partitioning.xml:379
#, no-c-format
msgid ""
"Several varieties of partitioning programs have been adapted by &debian; "
"developers to work on various types of hard disks and computer "
"architectures. Following is a list of the program(s) applicable for your "
"architecture."
msgstr ""

#. Tag: para
#: partitioning.xml:392
#, no-c-format
msgid ""
"Recommended partitioning tool in &debian;. This Swiss army knife can also "
"resize partitions, create filesystems <phrase arch=\"any-x86\"> "
"(<quote>format</quote> in Windows speak)</phrase> and assign them to the "
"mountpoints."
msgstr ""

#. Tag: para
#: partitioning.xml:404
#, no-c-format
msgid "The original Linux disk partitioner, good for gurus."
msgstr ""

#. Tag: para
#: partitioning.xml:408
#, no-c-format
msgid ""
"Be careful if you have existing FreeBSD partitions on your machine. The "
"installation kernels include support for these partitions, but the way that "
"<command>fdisk</command> represents them (or not) can make the device names "
"differ. See the <ulink url=\"&url-linux-freebsd;\">Linux+FreeBSD HOWTO</"
"ulink>."
msgstr ""

#. Tag: para
#: partitioning.xml:421
#, no-c-format
msgid "A simple-to-use, full-screen disk partitioner for the rest of us."
msgstr ""

#. Tag: para
#: partitioning.xml:425
#, no-c-format
msgid ""
"Note that <command>cfdisk</command> doesn't understand FreeBSD partitions at "
"all, and, again, device names may differ as a result."
msgstr ""

#. Tag: para
#: partitioning.xml:435
#, no-c-format
msgid "Atari-aware version of <command>fdisk</command>."
msgstr ""

#. Tag: para
#: partitioning.xml:444
#, no-c-format
msgid "Amiga-aware version of <command>fdisk</command>."
msgstr ""

#. Tag: para
#: partitioning.xml:453
#, no-c-format
msgid "Mac-aware version of <command>fdisk</command>."
msgstr ""

#. Tag: para
#: partitioning.xml:462
#, no-c-format
msgid ""
"PowerMac-aware version of <command>fdisk</command>, also used by BVM and "
"Motorola VMEbus systems."
msgstr ""

#. Tag: para
#: partitioning.xml:472
#, no-c-format
msgid ""
"&arch-title; version of <command>fdisk</command>; Please read the fdasd "
"manual page or chapter 13 in <ulink url=\"http://oss.software.ibm.com/"
"developerworks/opensource/linux390/docu/l390dd08.pdf\"> Device Drivers and "
"Installation Commands</ulink> for details."
msgstr ""

#. Tag: para
#: partitioning.xml:483
#, no-c-format
msgid ""
"One of these programs will be run by default when you select "
"<guimenuitem>Partition disks</guimenuitem> (or similar). It may be possible "
"to use a different partitioning tool from the command line on VT2, but this "
"is not recommended."
msgstr ""

#. Tag: para
#: partitioning.xml:490
#, no-c-format
msgid "Remember to mark your boot partition as <quote>Bootable</quote>."
msgstr ""

#. Tag: para
#: partitioning.xml:493
#, no-c-format
msgid ""
"One key point when partitioning for Mac type disks is that the swap "
"partition is identified by its name; it must be named <quote>swap</quote>. "
"All Mac linux partitions are the same partition type, Apple_UNIX_SRV2. "
"Please read the fine manual. We also suggest reading the <ulink url=\"&url-"
"mac-fdisk-tutorial;\">mac-fdisk Tutorial</ulink>, which includes steps you "
"should take if you are sharing your disk with MacOS."
msgstr ""

#. Tag: title
#: partitioning.xml:509
#, no-c-format
msgid "Partitioning for &arch-title;"
msgstr ""

#. Tag: para
#: partitioning.xml:510
#, no-c-format
msgid ""
"If you are using a new harddisk (or want to wipe out the whole partition "
"table of your disk), a new partition table needs to be created. The "
"<quote>Guided partitioning</quote> does this automatically, but when "
"partitioning manually, move the selection on the top-level entry of the disk "
"and hit &enterkey;. That will create a new partition table on that disk. In "
"expert mode, you will then be asked for the type of the partition table. "
"Default for UEFI-based systems is <quote>gpt</quote>, while for the older "
"BIOS world the default value is <quote>msdos</quote>. In a standard priority "
"installation those defaults will be used automatically."
msgstr ""

#. Tag: para
#: partitioning.xml:522
#, no-c-format
msgid ""
"When a partition table with type <quote>gpt</quote> was selected (default "
"for UEFI systems), a free space of 1 MB will automatically get created at "
"the beginning of the disk. This is intended and required to embed the GRUB2 "
"bootloader."
msgstr ""

#. Tag: para
#: partitioning.xml:529
#, no-c-format
msgid ""
"If you have an existing other operating system such as DOS or Windows and "
"you want to preserve that operating system while installing &debian;, you "
"may need to resize its partition to free up space for the &debian; "
"installation. The installer supports resizing of both FAT and NTFS "
"filesystems; when you get to the installer's partitioning step, select the "
"option <guimenuitem>Manual</guimenuitem> and then simply select an existing "
"partition and change its size."
msgstr ""

#. Tag: para
#: partitioning.xml:539
#, no-c-format
msgid ""
"While modern UEFI systems don't have such limitations as listed below, the "
"old PC BIOS generally adds additional constraints for disk partitioning. "
"There is a limit to how many <quote>primary</quote> and <quote>logical</"
"quote> partitions a drive can contain. Additionally, with pre 1994&ndash;98 "
"BIOSes, there are limits to where on the drive the BIOS can boot from. More "
"information can be found in the <ulink url=\"&url-partition-howto;\">Linux "
"Partition HOWTO</ulink>, but this section will include a brief overview to "
"help you plan most situations."
msgstr ""

#. Tag: para
#: partitioning.xml:550
#, no-c-format
msgid ""
"<quote>Primary</quote> partitions are the original partitioning scheme for "
"PC disks. However, there can only be four of them. To get past this "
"limitation, <quote>extended</quote> and <quote>logical</quote> partitions "
"were invented. By setting one of your primary partitions as an extended "
"partition, you can subdivide all the space allocated to that partition into "
"logical partitions. You can create up to 60 logical partitions per extended "
"partition; however, you can only have one extended partition per drive."
msgstr ""

#. Tag: para
#: partitioning.xml:561
#, no-c-format
msgid ""
"Linux limits the partitions per drive to 255 partitions for SCSI disks (3 "
"usable primary partitions, 252 logical partitions), and 63 partitions on an "
"IDE drive (3 usable primary partitions, 60 logical partitions). However the "
"normal &debian-gnu; system provides only 20 devices for partitions, so you "
"may not install on partitions higher than 20 unless you first manually "
"create devices for those partitions."
msgstr ""

#. Tag: para
#: partitioning.xml:571
#, no-c-format
msgid ""
"If you have a large IDE disk, and are using neither LBA addressing, nor "
"overlay drivers (sometimes provided by hard disk manufacturers), then the "
"boot partition (the partition containing your kernel image) must be placed "
"within the first 1024 cylinders of your hard drive (usually around 524 "
"megabytes, without BIOS translation)."
msgstr ""

#. Tag: para
#: partitioning.xml:579
#, no-c-format
msgid ""
"This restriction doesn't apply if you have a BIOS newer than around "
"1995&ndash;98 (depending on the manufacturer) that supports the "
"<quote>Enhanced Disk Drive Support Specification</quote>. &debian;'s Lilo "
"alternative <command>mbr</command> must use the BIOS to read the kernel from "
"the disk into RAM. If the BIOS int 0x13 large disk access extensions are "
"found to be present, they will be utilized. Otherwise, the legacy disk "
"access interface is used as a fall-back, and it cannot be used to address "
"any location on the disk higher than the 1023rd cylinder. Once &arch-kernel; "
"is booted, no matter what BIOS your computer has, these restrictions no "
"longer apply, since &arch-kernel; does not use the BIOS for disk access."
msgstr ""

#. Tag: para
#: partitioning.xml:593
#, no-c-format
msgid ""
"If you have a large disk, you might have to use cylinder translation "
"techniques, which you can set from your BIOS setup program, such as LBA "
"(Logical Block Addressing) or CHS translation mode (<quote>Large</quote>). "
"More information about issues with large disks can be found in the <ulink "
"url=\"&url-large-disk-howto;\">Large Disk HOWTO</ulink>. If you are using a "
"cylinder translation scheme, and the BIOS does not support the large disk "
"access extensions, then your boot partition has to fit within the "
"<emphasis>translated</emphasis> representation of the 1024th cylinder."
msgstr ""

#. Tag: para
#: partitioning.xml:605
#, no-c-format
msgid ""
"The recommended way of accomplishing this is to create a small "
"(25&ndash;50MB should suffice) partition at the beginning of the disk to be "
"used as the boot partition, and then create whatever other partitions you "
"wish to have, in the remaining area. This boot partition <emphasis>must</"
"emphasis> be mounted on <filename>/boot</filename>, since that is the "
"directory where the &arch-kernel; kernel(s) will be stored. This "
"configuration will work on any system, regardless of whether LBA or large "
"disk CHS translation is used, and regardless of whether your BIOS supports "
"the large disk access extensions."
msgstr ""
